The conDetSEC approach is implemented in the main_varyingLength.py file.
The Recurrent Neural Network autoencoder is implemented in the model.py file.

The main_varyingLength.py file takes as input three information:
- The Directory in which data are stored
- The number of labelled samples to derive MUST and CANNOT Link constraints (this is used to access to the related fiile)
- The run identifier to add to the output file name

In the data directory (i.e. JapWov) there are different files:
- data.npy: contains the data in numpy format as a tensor. (JapWov: (640, 29, 12) )
- class.npy: contains the label assignment. (JapWov: (640,) )
- seqLength.npy: contains the valid timestamps for each multi-variate time series contained in data.npy (JapWov: (640,)). The value of each row of this file is between 1 and the maximum lenght of the time series of the dataset


Example to run the code:
python main_varyingLength.py JapWov 10 0

This means that we run the clustering algorithm on the Japanese Wovel dataset, with a number of constraints derived using 10 labelled instances per classes. The final 0 indicates the run number in order to record which is the specific run (for experimental evaluation purposes).
Running this line of code makes the hypothesis that in the JapWov folder, the file 10_0.npy exists.
The file 10_0.npy contains the identifier (position) of 10 samples per class that will be used to derive constraints.

The "python main_varyingLength.py JapWov 10 0" line command will produce two files:
- JapWov/res_10_0.npy: The results of the K-Means clustering algorithm applied on the learnt embeddings
- JapWov/emb_10_0.npy: The data embedding produced by our framework

