import tensorflow as tf

class RNNAE(tf.keras.Model):
    def __init__(self, filters, outputDim, n_cluster, dropout_rate = 0.0, hidden_activation='relu', output_activation='softmax',
                 name='RNNAE',
                 **kwargs):
        # chiamata al costruttore della classe padre, Model
        super(RNNAE, self).__init__(name=name, **kwargs)
        self.encoder = tf.keras.layers.GRU(filters, return_sequences=True)
        self.encoderR = tf.keras.layers.GRU(filters, go_backwards=True, return_sequences=True)

        self.decoder = tf.keras.layers.GRU(filters, return_sequences=True)
        self.decoder2 = tf.keras.layers.TimeDistributed(tf.keras.layers.Dense(units=outputDim, activation=None))

        self.decoderR = tf.keras.layers.GRU(filters, return_sequences=True)
        self.decoder2R = tf.keras.layers.TimeDistributed(tf.keras.layers.Dense(units=outputDim, activation=None))

    def siameseDistance(self, inputs, training=False):
        first_elements = inputs[0]
        second_elements = inputs[1]

        femb = self.encF(first_elements, training=training)
        semb = self.encF(second_elements, training=training)
        d_W = tf.reduce_sum( tf.square(femb - semb), axis=1)
        return d_W

    def encF(self, inputs, training=False):
        seqEmb = self.encoder(inputs)
        seqEmbR = self.encoderR(inputs)
        emb = tf.unstack(seqEmb,axis=1)[-1]
        embR = tf.unstack(seqEmbR,axis=1)[-1]
        return emb+embR

    def decF(self, seq_emb, emb, training=False):
        dec = self.decoder(seq_emb)
        decR = self.decoderR(seq_emb)

        dec = self.decoder2(dec)
        decR = self.decoder2R(decR)
        decR = tf.reverse(decR, axis=[1])

        return dec, decR

    def call(self, inputs, training=False):
        t = inputs.get_shape()
        emb = self.encF(inputs, training)
        seq_emb = tf.keras.layers.RepeatVector(t[1])(emb)
        dec, decR = self.decF(seq_emb, emb, training)
        return emb, dec, decR

        #(dec+decR)/2, tf.concat((emb,embR),axis=1), tf.concat((emb,embR),axis=1), tf.concat((emb,embR),axis=1)
